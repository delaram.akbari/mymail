import React from 'react';
import './App.css';
import NavBar from './components/NavBar'
import SideBar from './components/Sidebar'
import { makeStyles } from '@material-ui/core/styles';
import ContentViewer from './components/ContentViewer';
import PropTypes from "prop-types";
// import {Route} from "react-router";

// import { BrowserRouter as Router, Route, Link } from "react-router-dom";



const useStyles = makeStyles(theme => ({
    wrapper: {
        height: '100%',
    },
}));
function App(props) {
    const classes = useStyles();

    return (
        <div>
            <div className={classes.wrapper}>
                <NavBar/>
            </div>
            <div>
                <SideBar/>
                {/*<ContentViewer mailbox={props.mailbox}/>*/}

                {props.children}
            </div>
        </div>
    );
}



App.propTypes = {
    mailbox: PropTypes.any.isRequired
};



export default App;
