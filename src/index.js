import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';


// import ReactDOM from 'react-dom';
import { Switch, Redirect, Route} from 'react-router';
import {BrowserRouter, Link} from 'react-router-dom';
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";

import ContentViewer from './components/ContentViewer';
// ReactDOM.render(<App />, document.getElementById('root'));



ReactDOM.render((
    <BrowserRouter>
        <App>
            <Switch>
                <Route exact path='/' render={()=><ContentViewer mailbox={'inbox'}/>}/>
                <Route exact path='/starred' render={()=><ContentViewer mailbox={'starred'}/>}/>
                <Route exact path='/sent' render={()=><ContentViewer mailbox={'sent'}/>}/>
            </Switch>
        </App>
    </BrowserRouter>
), document.getElementById('root'));


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();