import React, {useState} from 'react'
import List from "@material-ui/core/List";
import {makeStyles} from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';

import AddIcon from '@material-ui/icons/Add';
import InboxIcon from '@material-ui/icons/Inbox';
import StarIcon from '@material-ui/icons/Star';
import ImportantIcon from '@material-ui/icons/LabelImportantSharp';
import SendIcon from '@material-ui/icons/Send';
import DraftsIcon from '@material-ui/icons/InsertDriveFile';
import SnoozedIcon from '@material-ui/icons/AccessTime';
import MoreIcon from '@material-ui/icons/ExpandMore';
import LessIcon from '@material-ui/icons/ExpandLess';
import ChatsIcon from '@material-ui/icons/Textsms';
import AllMailIcon from '@material-ui/icons/Email';
import SpamIcon from '@material-ui/icons/ReportRounded';
import TrashIcon from '@material-ui/icons/Delete';
import CategoryIcon from '@material-ui/icons/LabelRounded';
import ScheduledIcon from '@material-ui/icons/Schedule';
import SettingIcon from '@material-ui/icons/SettingsRounded';

import MailBoxUI from './MailBoxUI'

// import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { Redirect } from 'react-router-dom'



const useStyles = makeStyles(theme => ({
    sidebarContent: {
        display: 'block',
        float: 'left',
        width: '250px',
        paddingLeft: '5px',
        paddingRight: '5px',
    },
    listItem: {
        borderRadius: '2rem',
        height: '30px',
        fontFamily: "Roboto",

    },
    listItemSelected: {
        borderRadius: '2rem',
        height: '30px',
        fontFamily: "Roboto",
        color: '#d93025',
        backgroundColor: 'rgba(255, 0, 0, 0.15)',
    },
    listItemText: {
        fontSize: '.875rem',
    },
    listItemIcon: {
        fontSize: '.875rem',
        paddingRight: '15px',
    },
    listItemIconSelected: {
        color: '#d93025',
    },
    fab: {
        margin: theme.spacing(1),
        fontSize: '12px',
        fontWeight: 'bold',
        fontFamily: "'Google Sans'",
        backgroundColor: 'White',
        textTransform: 'none',
        height: '50px',
        width: '150px',
        boxShadow: '0 3px 3px rgba(0,0,0,0.16), 0 3px 3px rgba(0,0,0,0.23)',
        letterSpacing: '1px',
    },
    extendedIcon: {
        marginLeft: theme.spacing(-1),
        color: 'red',
        fontSize: '40px',
    },

}));


function createmailbox(name, icon) {
    return {name, icon};
}

const mailBoxes = [
    createmailbox('Inbox', <InboxIcon/>),
    createmailbox('Starred', <StarIcon/>),
    createmailbox('Snoozed', <SnoozedIcon/>),
    createmailbox('Sent', <SendIcon/>),
    createmailbox('Drafts', <DraftsIcon/>),
    createmailbox('Less', <LessIcon/>),
    createmailbox('Important', <ImportantIcon/>),
    createmailbox('Chats', <ChatsIcon/>),
    createmailbox('Scheduled', <ScheduledIcon/>),
    createmailbox('All Mail', <AllMailIcon/>),
    createmailbox('Spam', <SpamIcon/>),
    createmailbox('Trash', <TrashIcon/>),
    createmailbox('Categories', <CategoryIcon/>),
    createmailbox('Manage labels', <SettingIcon/>),
    createmailbox('Create new label', <AddIcon/>),
];


const Sidebar = () => {

    const classes = useStyles();

    const [selectedBox, setSelectedBox] = useState(0);
    const [showAllBoxs, setShowAllBoxs] = useState(false);

    const [changeRoute, setChangeRoute] = useState(false);

    function handleBoxClick(index) {
        if (index !== selectedBox)
            setSelectedBox(index);
        setChangeRoute(true);
    }
    function handleMoreClick() {
        setShowAllBoxs(!showAllBoxs);
    }


    // if(changeRoute){
    //     return(<Redirect to={'//'+mailBoxes[selectedBox]}/>);
    // }

    let mlbx;

    if(showAllBoxs){
        mlbx = mailBoxes;
    }else{
        mlbx = mailBoxes.slice(0, 5);
        mlbx.push(createmailbox('More', <MoreIcon/>));
    }

    const renderMailBoxes = (
        mlbx.map((mailbox, index) => (
            <MailBoxUI selected={index === selectedBox} primary={index === 0} text={mailbox.name} icon={mailbox.icon} onClick={() => (index !== 5)?handleBoxClick(index):handleMoreClick()}/>
        ))
    );

    // const renderRoutes = (
    //     mlbx.map((mailbox, index) => (
    //         {/*<Route path={'/'+mailbox.name} />*/}
    //     ))
    // );

    return (

        <div className={classes.sidebarContent}>


            <Fab variant="extended" aria-label="delete" className={classes.fab} color={'white'}>
                <AddIcon className={classes.extendedIcon}/>
                Compose
            </Fab>


            <List>
                {renderMailBoxes}

            </List>
        </div>
    )
}

export default Sidebar;