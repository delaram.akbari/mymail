import React from 'react'

import SelectMailMenuUI from './SelectMailMenuUI'
import {makeStyles} from "@material-ui/core";
import Fab from "@material-ui/core/Fab";
import RefreshIcon from "@material-ui/icons/Refresh";


const useStyles = makeStyles(theme => ({
    ControlBarContent: {
        display: 'block',
        // float: 'left',
        width: '100%',
        paddingLeft: '5px',
        paddingRight: '5px',
    },
    fab:{
        backgroundColor: 'white',
        boxShadow: 'none',
        fontsize:'10px',
        margin: '8px',
        // width: '33px',
        // height: '30px',
        padding: '0px',
        '&:hover': {
            backgroundColor: '#0000000F',
        },
    },
    fabIcon:{
        fontsize: '15px',
    },
}));


const MailListControlBar = () =>{
    const classes = useStyles();
    return (
        <div className={classes.ControlBarContent}>
            <SelectMailMenuUI/>
            <Fab aria-label="refresh" size={'small'} className={classes.fab}>
                <RefreshIcon color={'action'}  className={classes.fabIcon}/>
            </Fab>
        </div>
    )
}

export default MailListControlBar;