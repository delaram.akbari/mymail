import React from 'react';
import {fade, makeStyles} from '@material-ui/core/styles'
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';
import Fab from '@material-ui/core/Fab';
import RefreshIcon from '@material-ui/icons/Refresh'

const useStyles = makeStyles(theme => ({

    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
        display: 'inline-block',
    },
    select:{
        paddingLeft: '0px',
    },
    margin: {
        margin: theme.spacing(1),
    },
    inputs:{
        backgroundColor: '#0000000F',
        '&:hover': {
            backgroundColor: '#000000FF',
        },
    }
}));

export default function SelectMailMenuUI() {
    const classes = useStyles();
    const [age, setAge] = React.useState('');
    const [open, setOpen] = React.useState(false);
    const [checkedF, setCheckedF] = React.useState(false);

    function handleChange(event) {
        setAge(event.target.value);
    }

    function handleCheckChange(){
        setCheckedF(!checkedF);
    }

    function handleClose() {
        setOpen(false);
    }

    function handleOpen() {
        setOpen(true);
    }

    return (
            <FormControl className={classes.formControl}>
                <Checkbox
                    checked={checkedF}
                    onChange={handleCheckChange}
                    value="checkedF"
                    indeterminate
                    inputProps={{
                        'aria-label': 'indeterminate checkbox',
                    }}
                />
                <Select
                    open={open}
                    onClose={handleClose}
                    onOpen={handleOpen}
                    value={''}
                    onChange={handleChange}
                    inputProps={{
                        name: 'age',
                        id: 'demo-controlled-open-select',
                    }}
                    className={classes.select}
                >
                    <MenuItem value="">
                        <em>None</em>
                    </MenuItem>
                    <MenuItem value={10}>Ten</MenuItem>
                    <MenuItem value={20}>Twenty</MenuItem>
                    <MenuItem value={30}>Thirty</MenuItem>
                </Select>



            </FormControl>

    );
}
