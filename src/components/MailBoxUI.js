import React from 'react'
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ListItem from "@material-ui/core/ListItem";
import PropTypes from "prop-types";
import {makeStyles} from "@material-ui/core";
import Link from '@material-ui/core/Link';


const useStyles = makeStyles(theme => ({
    sidebarContent: {
        display: 'block',
        float: 'left',
        width: '250px',
        paddingTop: '80px',
        paddingLeft: '5px',
        paddingRight: '5px',
    },
    listItem: {
        borderRadius: '2rem',
        height: '30px',
        fontFamily: "Roboto",

    },
    listItemSelected: {
        borderRadius: '2rem',
        fontweight: 'bolder',
        height: '30px',
        fontFamily: "Roboto",
        color: 'black',
        backgroundColor: 'rgba(0, 0, 0, 0.15)',
    },
    listItemSelectedPrimary: {
        borderRadius: '2rem',
        height: '30px',
        fontFamily: "Roboto",
        color: '#d93025',
        backgroundColor: 'rgba(255, 0, 0, 0.15)',
    },
    listItemText: {
        fontSize: '.875rem',
    },
    listItemIcon: {
        fontSize: '.875rem',
        paddingRight: '15px',
    },
    listItemIconSelected: {
        color: '#d93025',
    },
    fab: {
        margin: theme.spacing(1),
        fontSize: '12px',
        fontWeight: 'bold',
        fontFamily: "'Google Sans'",
        backgroundColor: 'White',
        textTransform: 'none',
        height: '50px',
        width: '150px',
        boxShadow: '0 3px 3px rgba(0,0,0,0.16), 0 3px 3px rgba(0,0,0,0.23)',
        letterSpacing: '1px',
    },
    extendedIcon: {
        marginLeft: theme.spacing(-1),
        color: 'red',
        fontSize: '40px',
    },

}));

const MailBoxUI = (props) => {
    const classes = useStyles();
    return (
        <ListItem
            className={[classes.listItem, (props.primary && props.selected ? classes.listItemSelectedPrimary : props.selected ? classes.listItemSelected : null)]}
            button key={props.text}  onClick={() => props.onClick()}>

            {/*<Link to={"/" + props.text}>*/}
                <ListItemIcon
                    className={[classes.listItemIcon, (props.selected ? classes.listItemIconSelected : null)]}>
                    {props.icon}
                </ListItemIcon>
                <ListItemText className={classes.listItemText} primary={props.text}/>
            {/*</Link>*/}
            {/*<Route path={'/'+props.text} />*/}
        </ListItem>
    )
}

MailBoxUI.propTypes = {
    selected: PropTypes.bool.isRequired,
    primary: PropTypes.bool.isRequired,
    text: PropTypes.string.isRequired,
};
export default MailBoxUI;