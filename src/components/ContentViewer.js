import React from 'react'
import MailListControlBar from './MailListControlBar'
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from "prop-types";
import NavBar from "./NavBar";



const useStyles = makeStyles(theme => ({
    tab:{
        width:'25%',
        '&:hover':{
            backgroundColor: '#0000000F',
        }
    }
}));

const ContentViewer = (props) =>{

    const classes = useStyles();
    const [value, setValue] = React.useState(0);


    function handleChange(event, newValue) {
        setValue(newValue);
    }

    return(
        <div>
            <MailListControlBar/>
            <div>
                <Tabs value={value} style={{width:'wrap-content'}} onChange={handleChange} aria-label="simple tabs example">
                    <Tab label="Primary"  className={classes.tab}/>
                    <Tab label="Social"  className={classes.tab}/>
                    <Tab label="Promotions" className={classes.tab}/>
                    <Tab label="Forums"  className={classes.tab} />
                </Tabs>
            </div>
            <div>
                <h1>{props.mailbox}</h1>
            </div>

        </div>
    )
}

ContentViewer.propTypes = {
    mailbox: PropTypes.string.isRequired
};

export default ContentViewer;